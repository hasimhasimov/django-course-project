from django.db import models

# Create your models here.
class Note(models.Model):
	STATUS_TYPE = (
		('tamamlanib', 'Tamamlanib'),
		('davam', 'Davam')
	)

	title = models.CharField(max_length=125)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)
	status = models.CharField(max_length=20, choices=STATUS_TYPE)
