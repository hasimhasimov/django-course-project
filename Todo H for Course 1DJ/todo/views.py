from django.shortcuts import render, redirect
from .models import Note

# Create your views here.
def home(requests):
	if requests.method == 'POST':
		note = requests.POST['not']
		yeni_note = Note(title=note)
		yeni_note.save()
		return redirect('home')
	else:
		notlar = Note.objects.all().order_by('-id')
		context = {
			'notlar': notlar
		}
		return render(requests, 'todo/home.html', context)


def edit(requests, pk):
	note = Note.objects.get(id=pk)
	if requests.method == 'GET':
		context = {
			'note': note
		}
		return render(requests, 'todo/edit.html', context)
	else:
		basliq = requests.POST['not']
		note.title = basliq
		note.save()
		return redirect('home')


def delete(requests, pk):
	note = Note.objects.get(id=pk)
	if requests.method == 'GET':
		context = {
			'note':note
		}
		return render(requests, 'todo/delete.html', context)
	else:
		note.delete()
		return redirect('home')

def tamam(requests, pk):
	note = Note.objects.get(id=pk)
	note.status = 'tamamlanib'
	note.save()
	return redirect('home')